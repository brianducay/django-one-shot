from django.shortcuts import render
from django.shortcuts import redirect
from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy

# Create your views here.
class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    # success_url = reverse_lazy("todo_list_detail")

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/items/create.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todo_list_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    # def get_success_url(self):
    #     return reverse_lazy("todo_list_list")


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]
    # success_url = reverse_lazy("todo_list_detail")

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])
